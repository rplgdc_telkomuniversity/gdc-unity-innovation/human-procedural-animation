![Movie](https://gitlab.com/rplgdc_telkomuniversity/gdc-unity-innovation/human-procedural-animation/uploads/51e535e51974436b7289843055b6c519/2021-04-30_20-59-08.mp4)

## Contributor

| Profile | 
| ------ |
| [![Firdiar](https://gitlab.com/uploads/-/system/user/avatar/2307294/avatar.png?raw=true)](https://www.linkedin.com/in/firdiar) |
| [Firdiansyah Ramadhan](https://www.linkedin.com/in/firdiar) | 

## License

MIT

**Free Software, Hell Yeah!**
