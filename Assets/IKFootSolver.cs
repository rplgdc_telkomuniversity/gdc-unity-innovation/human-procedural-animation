﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKFootSolver : MonoBehaviour
{

    [SerializeField] LayerMask terrainLayer = default;

    [SerializeField] Transform myFoot = default;
    [SerializeField] Transform graphicRoot = default;
    
    [SerializeField] Transform body = default;
    
    [SerializeField] IKFootSolver otherFoot = default;

    [SerializeField] float stepDistance = 3;
    [SerializeField] float diffHeight = 0.2f;
    [SerializeField] float upDownSpeed = 5;
    [SerializeField] bool isLeftFoot = false;

    float deffaultHeight = 0;

    float skinWidth = 0.8f;
    float heighOffset = 0;
    float steppingOffset = 0;

    float lerp = 0;

    Vector3 belowFootPosition = Vector3.zero;
    Vector3 prevFootPosition = Vector3.zero;
    Vector3 holdPos = Vector3.zero;


    float timeLimit = 0;


    float FootToTarget => (belowFootPosition.y - myFoot.position.y);
    float FootToBody => (graphicRoot.position.y - myFoot.position.y);
    private void Start()
    {
        deffaultHeight = (graphicRoot.position - myFoot.position).y;
        steppingOffset =  myFoot.position.x - body.position.x;
        heighOffset = (body.position.y - body.localScale.y- skinWidth) - myFoot.position.y;
        lerp = 1;

        belowFootPosition = transform.position;
        prevFootPosition = transform.position;
        holdPos = transform.position;
    }


    void Update()
    {

        Ray ray = new Ray(body.position + (body.right * steppingOffset) , Vector3.down);
        bool isHit = Physics.Raycast(ray, out RaycastHit info, 10, terrainLayer.value);

        if (isHit)
        {
            belowFootPosition = info.point + new Vector3(0, heighOffset, 0);
            timeLimit += Time.deltaTime;

            if (Vector3.Distance(prevFootPosition, info.point) > stepDistance && timeLimit > 1)
            {
                //lerp = 0;
                Vector3 delta = (belowFootPosition - prevFootPosition);

                //newPosition = info.point + (body.forward * direction);
                //newNormal = info.normal;

                holdPos = belowFootPosition + (delta);

                prevFootPosition = holdPos;
                timeLimit = 0;
            }


            #region BodyUpDown
            if (FootToTarget < -diffHeight)//if the lowest foot can't step the ground
            {
                Debug.Log("Down");
                MoveGraphicRoot(FootToTarget * Time.deltaTime * upDownSpeed);
            }

            if (belowFootPosition.y <= otherFoot.belowFootPosition.y)//if only this foot was the lowest
            {
                float delta = deffaultHeight - FootToBody;
                if (delta > diffHeight)
                {
                    Debug.Log("Up");
                    MoveGraphicRoot(delta * Time.deltaTime * upDownSpeed);
                }
            }
            #endregion
        }




        if (lerp < 1)
        {
            
        }
        else
        {
            
        }

        transform.position = holdPos;
    }


    private void MoveGraphicRoot(float y) {
        Vector3 pos = graphicRoot.position;
        pos.y += y;
        graphicRoot.position = pos;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(belowFootPosition, 0.3f);
    }

    public bool IsMoving()
    {
        return lerp < 1;
    }



}
