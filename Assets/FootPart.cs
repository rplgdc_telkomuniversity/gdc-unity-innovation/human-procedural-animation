﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootPart : MonoBehaviour
{
    [SerializeField] LayerMask terrainLayer = default;

    float steppingOffset = 0;
    float heighOffset = 0;
    




    public void Initialize(Transform graphicFoot , Transform body)
    {
        steppingOffset = graphicFoot.position.x - body.position.x;
        heighOffset = (graphicFoot.position.y);
    }

    // Update is called once per frame
    public Vector3 GetHit(Transform body , Vector3 directionMovement ,float steppingDistance)
    {
        Ray ray = new Ray(body.position + (body.right * steppingOffset) + (directionMovement*steppingDistance) , Vector3.down);
        if (Physics.Raycast(ray, out RaycastHit info, 10, terrainLayer.value))
        {
            return info.point + new Vector3(0, heighOffset, 0);
        }
        else {
            return transform.position;
        }
    }

}
