﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralWalkAnimation : MonoBehaviour
{
    [Header("Graphic Transform")]
    [SerializeField] Transform bodyGraphic = default;
    [SerializeField] Transform leftFootGraphic = default;
    [SerializeField] Transform rightFootGraphic = default;

    [Header("Object Transform")]
    [SerializeField] Transform bodyObject = default;
    [SerializeField] FootPart leftFootIK = default;
    [SerializeField] FootPart rightFootIK = default;


    [Header("Stepping Atribute")]
    [SerializeField] LayerMask terrainLayer = default;
    [SerializeField] MinMax steppingDistance;
    [SerializeField] MinMax movingVelocity;
    [SerializeField] AnimationCurve interpolation;
    [SerializeField] float horizontalMovementBonus = 0.4f;
    [SerializeField] float stepHeight;
    [SerializeField] float animateSpeed = 10;
    [SerializeField] float cooldown = 1;

    [Header("UpDown Attribute")]
    //[SerializeField] float diffHeighTolerance = 0.5f;
    [SerializeField] float bodyIdleHeight = 0f;
    [SerializeField] float bodyRunHeight = 0.5f;
    [SerializeField] float bodySquatHeight = 0.5f;


    [Header("Only to show")]
    [SerializeField] float _velocity = 0;
    [SerializeField] bool isIdle = true;
    [SerializeField] float bodyNormalHeight = 0.2f;


    Vector3 bodyTargetPos = default;
    Vector3 leftFootTargetPos = default;
    Vector3 rightFootTargetPos = default;
    Vector3 leftFootOldPos = default;
    Vector3 rightFootOldPos = default;
    bool leftLock, rightLock;
    float lerpLeft, lerpRight;

    float heightAdjustment = 0;

    Vector3 prevPosVelocity = Vector3.zero;
    Vector3 prevStep = Vector3.zero;

    bool isCurrentRight = true;
    int inMiddle = 0;

    float stepCooldown = 0;
    //float _footToBody = 0;
    //float FootToBodyL => bodyTargetPos.y - leftFootTargetPos.y;
    //float FootToBodyR => bodyTargetPos.y - rightFootTargetPos.y;

    float SteppingDistance {
        get {

            float iLerp = Mathf.InverseLerp(movingVelocity.min, movingVelocity.max , Velocity);
            return Mathf.Lerp(steppingDistance.min , steppingDistance.max , iLerp);
        }
    }

    [SerializeField]
    float Velocity
    {
        get
        {
            return _velocity;
        }
        set
        {
            _velocity = value;
            isIdle = (_velocity == 0);
            if (isIdle && !(inMiddle < 2))
            {
                heightAdjustment = bodyIdleHeight;
            }
            else if(!isIdle && (inMiddle<2))
            {
                heightAdjustment = bodyRunHeight;
            }
        }
    }


    [System.Serializable]
    public class MinMax {
        public float min;
        public float max;
    }

    // Start is called before the first frame update
    void Start()
    {

        leftFootIK.Initialize(leftFootGraphic, bodyObject);
        rightFootIK.Initialize(rightFootGraphic,  bodyObject);

        bodyTargetPos = bodyGraphic.position;
        leftFootTargetPos = leftFootIK.transform.position;
        rightFootTargetPos = rightFootIK.transform.position;

        bodyNormalHeight = (bodyTargetPos.y - leftFootTargetPos.y);
        heightAdjustment = bodyIdleHeight;

        prevPosVelocity = bodyObject.position;
        prevStep = bodyObject.position;

    }

    // Update is called once per frame
    void Update()
    {
        Velocity = (bodyObject.position - prevPosVelocity).magnitude / Time.deltaTime;
        prevPosVelocity = bodyObject.position;

        CalculateStepDistance();
        CalculateHeightBody();
        ProceedToTarget();

        if (Input.GetKeyDown(KeyCode.I))
            heightAdjustment = bodyIdleHeight;
        if (Input.GetKeyDown(KeyCode.O))
            heightAdjustment = bodyRunHeight;
        if (Input.GetKeyDown(KeyCode.P))
            heightAdjustment = bodySquatHeight;
    }


    void ProceedToTarget() {
        bodyGraphic.position = Vector3.Lerp(bodyGraphic.position, bodyTargetPos, Time.deltaTime* animateSpeed/2);

        if (leftLock)
            leftFootIK.transform.position = leftFootTargetPos;
        else
        {
            float divider = inMiddle < 1 ? 1 : 4; 
            lerpLeft = Mathf.Min( 1, lerpLeft +  (Time.deltaTime * animateSpeed/divider));
            leftFootIK.transform.position = GetPosition(lerpLeft , leftFootOldPos , leftFootTargetPos);

            if (lerpLeft == 1)
                leftLock = true;
        }

        if (rightLock)
            rightFootIK.transform.position = rightFootTargetPos;
        else
        {
            float divider = inMiddle < 1 ? 1 : 4;
            lerpRight = Mathf.Min(1, lerpRight + (Time.deltaTime * animateSpeed/divider));
            rightFootIK.transform.position = GetPosition(lerpRight , rightFootOldPos , rightFootTargetPos);
            if (lerpRight == 1)
                rightLock = true;
        }
        
    }

    Vector3 GetPosition(float lerp , Vector3 old, Vector3 target) {
        float lval = interpolation.Evaluate(lerp);
        Vector3 tempPosition = Vector3.Lerp(old, target, lval);
        tempPosition.y += Mathf.Sin(lerp * Mathf.PI) *stepHeight;
        return tempPosition;
    }


    void CalculateStepDistance() 
    {
        stepCooldown = Mathf.Max(0, stepCooldown - Time.deltaTime);
        if (!isIdle)
        {
            inMiddle = 0;
            float deltaMovement = (bodyObject.position - prevStep).magnitude;
            if (deltaMovement > SteppingDistance)
            {
                Step();
            }
        }
        else if (inMiddle < 2){

            if (stepCooldown > 0)
                return;

            stepCooldown = cooldown;
            inMiddle += 1;
            if (isCurrentRight)
            {
                lerpRight = 0;
                rightFootOldPos = rightFootTargetPos;
                rightFootTargetPos = rightFootIK.GetHit(bodyObject, Vector3.zero, 0);
            }
            else
            {
                lerpLeft = 0;
                leftFootOldPos = leftFootTargetPos;
                leftFootTargetPos = leftFootIK.GetHit(bodyObject, Vector3.zero, 0);
            }
            isCurrentRight = !isCurrentRight;


        }
    }

    
    void Step() {

        if (stepCooldown > 0) {
            return;
        }

        stepCooldown = cooldown;

        Debug.Log("Step");
        Vector3 movementDirection = bodyObject.position - prevStep;
        prevStep = bodyObject.position;
        if (isCurrentRight)
        {
            rightLock = false;
            lerpRight = 0;
            rightFootOldPos = rightFootTargetPos;
            float dotValue = Vector3.Dot(bodyObject.right, movementDirection.normalized);
            rightFootTargetPos =  rightFootIK.GetHit(bodyObject , movementDirection.normalized, SteppingDistance + (SteppingDistance* Mathf.Abs(dotValue)* horizontalMovementBonus));
        }
        else 
        {
            leftLock = false;
            lerpLeft = 0;
            leftFootOldPos = leftFootTargetPos;
            float dotValue = Vector3.Dot(bodyObject.right, movementDirection.normalized);
            leftFootTargetPos = leftFootIK.GetHit(bodyObject, movementDirection.normalized, SteppingDistance + (SteppingDistance * Mathf.Abs(dotValue) * horizontalMovementBonus));
        }

        isCurrentRight = !isCurrentRight;
    }

    void CalculateHeightBody() {

        bodyTargetPos = bodyObject.position;
        if (rightFootTargetPos.y < leftFootTargetPos.y)
        {
            bodyTargetPos.y = bodyNormalHeight + rightFootTargetPos.y + heightAdjustment;
        }
        else if (rightFootTargetPos.y > leftFootTargetPos.y)
        {
            bodyTargetPos.y = bodyNormalHeight + leftFootTargetPos.y + heightAdjustment;
        }

    }




    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(rightFootTargetPos, 0.4f);
        Gizmos.DrawSphere(leftFootTargetPos, 0.4f);
    }

}
